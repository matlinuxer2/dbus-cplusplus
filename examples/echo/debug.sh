#!/bin/bash

cat > ./test.gdb <<EOF
set break pending on
set pagination off
#set logging on ./test.log.gdb
set verbose off

# set the most earlier entry point
#break main 

run echo-client-mt

# read the generated breakpoints
#source ./test.bps 

#continue

bt

quit
EOF

gdb -batch -x ./test.gdb bash
rm ./test.gdb
