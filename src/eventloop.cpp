/*
 *
 *  D-Bus++ - C++ bindings for D-Bus
 *
 *  Copyright (C) 2005-2007  Paolo Durante <shackan@gmail.com>
 *
 *
 *  This library is free software; you can redistribute it and/or
 *  modify it under the terms of the GNU Lesser General Public
 *  License as published by the Free Software Foundation; either
 *  version 2.1 of the License, or (at your option) any later version.
 *
 *  This library is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 *  Lesser General Public License for more details.
 *
 *  You should have received a copy of the GNU Lesser General Public
 *  License along with this library; if not, write to the Free Software
 *  Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <dbus-c++/eventloop.h>
#include <dbus-c++/debug.h>

#include <sys/time.h>

#include <dbus/dbus.h>
#include "threadpool.h"

using namespace DBus;

static double millis(timeval tv)
{
	return (tv.tv_sec *1000.0 + tv.tv_usec/1000.0);
}
	
DefaultTimeout::DefaultTimeout(int interval, bool repeat, DefaultMainLoop *ed)
: _enabled(true), _interval(interval), _repeat(repeat), _expiration(0), _data(0), _disp(ed)
{
	timeval now;
	gettimeofday(&now, NULL);

	_expiration = millis(now) + interval;

	_disp->_mutex_t.lock();
	_disp->_timeouts.push_back(this);
	_disp->_mutex_t.unlock();
}

DefaultTimeout::~DefaultTimeout()
{
	_disp->_mutex_t.lock();
	_disp->_timeouts.remove(this);
	_disp->_mutex_t.unlock();
}

DefaultWatch::DefaultWatch(int fd, int flags, DefaultMainLoop *ed)
: _enabled(true), _fd(fd), _flags(flags), _state(0), _data(0), _disp(ed)
{
	_disp->_mutex_w.lock();
	_disp->_watches.push_back(this);
	_disp->_mutex_w.unlock();
}

DefaultWatch::~DefaultWatch()
{
	_disp->_mutex_w.lock();
	_disp->_watches.remove(this);
	_disp->_mutex_w.unlock();
}

DefaultMutex::DefaultMutex()
{
	pthread_mutex_init(&_mutex, NULL);
}

DefaultMutex::DefaultMutex(bool recursive)
{
	if (recursive)
	{
		pthread_mutex_t recmutex = PTHREAD_RECURSIVE_MUTEX_INITIALIZER_NP; 
 		_mutex = recmutex;
	}
	else
	{
		pthread_mutex_init(&_mutex, NULL);
	}
}

DefaultMutex::~DefaultMutex()
{
	pthread_mutex_destroy(&_mutex);
}

void DefaultMutex::lock()
{
	pthread_mutex_lock(&_mutex);
}

void DefaultMutex::unlock()
{
	pthread_mutex_unlock(&_mutex);
}

DefaultMainLoop::DefaultMainLoop()
{
}

DefaultMainLoop::~DefaultMainLoop()
{
	_mutex_w.lock();

	DefaultWatches::iterator wi = _watches.begin();
	while (wi != _watches.end())
	{
		DefaultWatches::iterator wmp = wi;
		++wmp;
		_mutex_w.unlock();
		delete (*wi);
		_mutex_w.lock();
		wi = wmp;
	}
	_mutex_w.unlock();

	_mutex_t.lock();

	DefaultTimeouts::iterator ti = _timeouts.begin();
	while (ti != _timeouts.end())
	{
		DefaultTimeouts::iterator tmp = ti;
		++tmp;
		_mutex_t.unlock();
		delete (*ti);
		_mutex_t.lock();
		ti = tmp;
	}
	_mutex_t.unlock();
}

void DefaultMainLoop::dispatch()
{
	debug_log("===[1]===");
	_mutex_w.lock();
	pthread_mutex_lock( &mutex_myPool2 );

	int nfd = _watches.size();

#ifndef _WIN32
	if(_fdunlock)
	{
		nfd=nfd+2;
	}
#endif

	DBusPollFD fds[nfd];

	DefaultWatches::iterator wi = _watches.begin();

	debug_log("===[2]===");
	for (nfd = 0; wi != _watches.end(); ++wi)
	{
		if ((*wi)->enabled())
		{
			fds[nfd].fd = (*wi)->descriptor();
			fds[nfd].events = (*wi)->flags();
			fds[nfd].revents = 0;

			++nfd;
		}
	}
	debug_log("===[3]===");

#ifndef _WIN32
	if(_fdunlock){
		fds[nfd].fd = _fdunlock[0];
		fds[nfd].events = _DBUS_POLLIN | _DBUS_POLLOUT | _DBUS_POLLPRI ;
		fds[nfd].revents = 0;
		
		nfd++;
		fds[nfd].fd = _fdunlock[1];
		fds[nfd].events = _DBUS_POLLIN | _DBUS_POLLOUT | _DBUS_POLLPRI ;
		fds[nfd].revents = 0;
	}
#endif

	debug_log("===[4]===");
	pthread_mutex_unlock( &mutex_myPool2 );
	_mutex_w.unlock();
	debug_log("===[5]===");

	int wait_min = 10000;

	DefaultTimeouts::iterator ti;

	debug_log("===[6]===");
	_mutex_t.lock();
	debug_log("===[7]===");

	for (ti = _timeouts.begin(); ti != _timeouts.end(); ++ti)
	{
		if ((*ti)->enabled() && (*ti)->interval() < wait_min)
			wait_min = (*ti)->interval();
	}

	debug_log("===[8]===");
	_mutex_t.unlock();
	debug_log("===[9]===");

	_dbus_poll(fds, nfd, wait_min);

	debug_log("===[10]===");
	timeval now;
	gettimeofday(&now, NULL);

	double now_millis = millis(now);

	debug_log("===[11]===");
	_mutex_t.lock();

	debug_log("===[12]===");
	ti = _timeouts.begin();

	while (ti != _timeouts.end())
	{
		DefaultTimeouts::iterator tmp = ti;
		++tmp;

		if ((*ti)->enabled() && now_millis >= (*ti)->_expiration)
		{
			(*ti)->expired(*(*ti));

			if ((*ti)->_repeat)
			{
				(*ti)->_expiration = now_millis + (*ti)->_interval;
			}

		}

		ti = tmp;
	}

	debug_log("===[13]===");
	_mutex_t.unlock();

	debug_log("===[14]===");
	_mutex_w.lock();

	debug_log("===[15]===");
	pthread_mutex_lock( &mutex_myPool2 );
	for (int j = 0; j < nfd; ++j)
	{
		DefaultWatches::iterator wi;

		for (wi = _watches.begin(); wi != _watches.end();)
		{
			DefaultWatches::iterator tmp = wi;
			++tmp;

			debug_log("mainlooping...");
			if ((*wi)->enabled() && (*wi)->_fd == fds[j].fd)
			{
				debug_log("filtering...%d", (*wi)->_fd );
				if (fds[j].revents)
				{
					debug_log("triggering...");
					(*wi)->_state = fds[j].revents;

					_mutex_t.lock();
					(*wi)->ready(*(*wi));
					_mutex_t.unlock();

					fds[j].revents = 0;
				}
			}

			wi = tmp;
		}
	}
	pthread_mutex_unlock( &mutex_myPool2 );
	debug_log("===[16]===");
	_mutex_w.unlock();
	debug_log("===[17]===");
}


#ifdef _WIN32
#include <winsock2.h>
#endif

int _dbus_poll (DBusPollFD *fds, int n_fds, int timeout_milliseconds)
{

	fd_set read_set, write_set, err_set;
	int max_fd = 0;
	int i;
	struct timeval tv;
	int ready;

	FD_ZERO (&read_set);
	FD_ZERO (&write_set);
	FD_ZERO (&err_set);

	for (i = 0; i < n_fds; i++)
	{
		DBusPollFD *fdp = &fds[i];

#ifdef _WIN32
		// skip file descriptors which is not socket.
		int optlen=4; char optval[optlen];
		if ( getsockopt( fdp->fd, SOL_SOCKET, SO_TYPE, optval, &optlen ) != 0 ){
			debug_log("ignoring (fd: %d) for selecting set...\n", fdp->fd  );
			continue;
		}
#endif

		debug_log("adding (fd: %d) for selecting set...\n", fdp->fd  );

		if (fdp->events & _DBUS_POLLIN){
			FD_SET (fdp->fd, &read_set);
		}

		if (fdp->events & _DBUS_POLLOUT){
			FD_SET (fdp->fd, &write_set);
		}

		FD_SET (fdp->fd, &err_set);

		max_fd = ( max_fd > fdp->fd ) ? max_fd : fdp->fd;
	}

	tv.tv_sec = timeout_milliseconds / 1000;
	tv.tv_usec = (timeout_milliseconds % 1000) * 1000;

	debug_log("before selecting...(max_fd: %d, tv_sec:%d, tv_usec: %d) \n", max_fd, tv.tv_sec, tv.tv_usec );
	ready = select (max_fd + 1, &read_set, &write_set, &err_set, timeout_milliseconds < 0 ? NULL : &tv);
	debug_log("after selecting (return: %d)... \n", ready );

	if (ready > 0)
	{
		debug_log("select: ready for triggering \n");
		for (i = 0; i < n_fds; i++)
		{
			DBusPollFD *fdp = &fds[i];

			fdp->revents = 0;

			if (FD_ISSET (fdp->fd, &read_set)){
				fdp->revents |= _DBUS_POLLIN;
			}

			if (FD_ISSET (fdp->fd, &write_set)){
				fdp->revents |= _DBUS_POLLOUT;
			}

			if (FD_ISSET (fdp->fd, &err_set)){
				fdp->revents |= _DBUS_POLLERR;
			}
		}
	}
	else if (ready == 0)
	{
		debug_log("select timeout \n"); 
	}
	else 
	{
		debug_log( "select failed!\n" ); 
#ifdef _WIN32
		int error_number = WSAGetLastError();
		debug_log("select: error number is ( %d )\n", error_number ); 
#endif
	}

	return ready;
}
